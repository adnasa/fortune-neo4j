const { oneLine } = require('common-tags');
const neo4j = require('neo4j-driver').v1;
const { withMatch, withWhere } = require('./utils/with');

const createNodeSelection = require('./utils/create-node-selection');
const createWhereClause = require('./utils/create-where-clause/create-where-clause.js');
const createRangeClause = require('./utils/create-range-clause/create-range-clause.js');

function generateId() {
  return crypto.randomBytes(15).toString(bufferEncoding);
}

module.exports = Adapter =>
  class Neo4jAdapter extends Adapter {
    connect() {
      const options = this.options;
      if (!options.credentials.name || !options.credentials.password)
        throw new Error('credentials are required');

      const driver = neo4j.driver(
        options.url,
        neo4j.auth.basic(options.credentials.name, options.credentials.password)
      );

      this.driver = driver;
      this.generateId = options.generateId || generateId;
      return Promise.resolve(this.driver);
    }

    disconnect() {
      try {
        this.driver.close();
        return Promise.resolve();
      } catch (error) {
        return Promise.reject(error);
      }
    }

    // @TODO: introduce `relationship`
    find(type, ids, options = {}) {
      if (ids && !ids.length) return super.find();

      const where =
        ids && ids.length
          ? ids.map(
              id =>
                options.strictMatch
                  ? `ID(${type}) = ${id}`
                  : `${type}.${this.keys.primary} = ${id}`
            )
          : [];
      const orderBy = [];
      let nodeMatch;

      let parameters = [];
      const Promise = this.Promise;
      const fieldDefinitions = this.recordTypes[type];
      const node = createNodeSelection(type, type, options.match);
      nodeMatch = node.selection;
      parameters.push(node.context);

      if (options) {
        if (options.match) {
          const matchWhere = createWhereClause(type, options.match);
          where.push(matchWhere.clause);
          parameters.push(matchWhere.context);
        }

        if (options.exists) {
          Object.keys(options.exists).forEach(existField => {
            const value = Boolean(options.exists[existField]);
            where.push(oneLine`
              ${type}.${existField}
              ${value ? 'IS NOT NULL' : 'IS NULL'}
            `);
            if (fieldDefinitions[existField][this.keys.isArray])
              where.push(
                `LENGTH(${type}.${existField}) ${value ? '> 0' : '= 0'}`
              );
          });
        }

        if (options.range) {
          const rangeClause = createRangeClause(type, options.range);
          where.push(rangeClause.clause);
          parameters.push(rangeClause.context);
        }

        if (options.sort) {
          Object.keys(options.sort).forEach(sortField => {
            const sortOrder = options.sort[sortField] ? 'ASC' : 'DESC';
            orderBy.push(`${type}.${sortField} ${sortOrder}`);
          });
        }
      }

      const MATCH_CLAUSE = withMatch(nodeMatch);
      const WHERE_CLAUSE = where.filter(x => !['', null, undefined].includes(x))
        .length
        ? withWhere(where.join(' AND '))
        : '';
      const query = `${MATCH_CLAUSE} ${WHERE_CLAUSE} RETURN ${type}`;
      const session = this.driver.session();
      return session
        .run(
          query,
          parameters.reduce((acc, p) => Object.assign({}, acc, p), {})
        )
        .then(results => {
          session.close();
          return results;
        })
        .then(results =>
          results.records.map(record => record.toObject()[type].properties)
        );
    }
    create(type, records) {
      return this.merge(type, records);
    }
    update(type, records) {
      return this.merge(type, records);
    }
    merge(type, records) {
      const recordsAsArray = Array.isArray(records) ? records : [records];
      if (recordsAsArray.length === 0) return Promise.resolve([]);
      const session = this.driver.session();
      const transaction = session.beginTransaction();

      const nodeSelections = recordsAsArray.map((record, index) => {
        const node = createNodeSelection(`${type}${index}`, type, record);
        return node;
      });
      const createdNodes = nodeSelections.map((createNode, index) =>
        transaction.run(
          `MERGE ${createNode.selection} RETURN ${type}${index}`,
          createNode.context
        )
      );
      return transaction.commit().then(results => {
        session.close();
        return results;
      });
    }
    delete(type, ids) {
      const whereClause =
        ids && ids.length
          ? `WHERE n.${this.keys.primary} in [${ids.join(', ')}]`
          : '';

      const session = this.driver.session();
      return session
        .run(
          oneLine`
            MATCH (n:${type})
            ${whereClause}
            DETACH DELETE n
          `
        )
        .then((...args) => {
          session.close();
          return args;
        });
    }
  };
