const neo4jAdapter = require('./index');
const config = require('../config');

const coreAdapter = require('fortune/lib/adapter');
const coreAdapterSingleton = require('fortune/lib/adapter/singleton');
const common = require('fortune/lib/common');
const errors = require('fortune/lib/common/errors');
const message = require('fortune/lib/common/message');
const deepEqual = require('fortune/lib/common/deep_equal');
const map = require('fortune/lib/common/array/map');
const find = require('fortune/lib/common/array/find');
const includes = require('fortune/lib/common/array/includes');
const filter = require('fortune/lib/common/array/filter');

const keys = require('fortune/lib/common/keys');
const denormalizedInverseKey = keys.denormalizedInverse;
const primaryKey = keys.primary;

const type = 'user';

const recordTypes = {
  user: {
    name: { type: String },
    age: { type: Integer },
    isAlive: { type: Boolean },
    birthday: { type: Date },
    junk: { type: Object },
    picture: { type: Buffer },
    privateKeys: { type: Buffer, isArray: true },
    nicknames: { type: String, isArray: true },
    friends: { link: 'user', isArray: true, inverse: 'friends' },
    nemesis: { link: 'user', inverse: '__user_nemesis_inverse' },
    __user_nemesis_inverse: {
      link: 'user',
      isArray: true,
      inverse: 'nemesis',
      [denormalizedInverseKey]: true,
    },
    bestFriend: { link: 'user', inverse: 'bestFriend' },
  },
};

const inputRecords = [
  {
    age: 42,
    bestFriend: 2,
    friends: [2],
    id: 1,
    isAlive: true,
    name: 'bob',
  },
  {
    age: 36,
    bestFriend: 1,
    friends: [1],
    id: 2,
    isAlive: false,
    name: 'john',
  },
];

describe('find', () => {
  let records;
  let withAdapter;
  beforeAll(() => {
    withAdapter = setup();
    withAdapter.then(adapter => {
      adapter.create(type, inputRecords);
      return adapter;
    });
  });
  describe('when fetching without ids', () => {
    beforeEach(() => {
      records = withAdapter.then(adapter => adapter.find(type, []));
    });
    it('should return a record list', () => {
      expect(records).resolves.toEqual(expect.any(Array));
    });
    it('should return an empty record list', () => {
      expect(records).resolves.toHaveLength(0);
    });
  });
  describe('match', () => {
    describe('when matching by multiple name and age', () => {
      beforeEach(() => {
        records = withAdapter.then(adapter =>
          adapter.find(type, null, {
            match: { name: ['john', 'xyz'], age: 36 },
          })
        );
      });
      it('should find `john`', () => {
        expect(records).resolves.toEqual([
          expect.objectContaining({
            name: 'john',
          }),
        ]);
      });
    });
    describe('when name is `bob` and age is `36`', () => {
      beforeEach(() => {
        records = withAdapter.then(adapter =>
          adapter.find(type, null, { match: { name: 'bob', age: 36 } })
        );
      });
      it('should not find any record', () => {
        expect(records).resolves.toHaveLength(0);
      });
    });
    describe('by name', () => {
      beforeEach(() => {
        records = withAdapter.then(adapter =>
          adapter.find(type, null, {
            match: {
              name: 'john',
            },
          })
        );
      });
      it('should find `john`', () => {
        expect(records).resolves.toEqual([
          expect.objectContaining({ name: 'john' }),
        ]);
      });
    });
  });
  describe('by id', () => {
    beforeEach(() => {
      records = withAdapter.then(adapter => adapter.find(type, [1]));
    });
    it('should have `bob` among the records', () => {
      expect(records).resolves.toEqual([
        expect.objectContaining({
          id: 1,
          name: 'bob',
        }),
      ]);
    });
  });
  describe('by type', () => {
    beforeEach(() => {
      records = withAdapter.then(adapter => adapter.find(type));
    });
    it('should and `bob` and `john` among the records', () => {
      expect(records).resolves.toEqual([
        expect.objectContaining({ id: 1 }),
        expect.objectContaining({ id: 2 }),
      ]);
    });
  });
  describe('range', () => {
    describe('number', () => {
      describe('when `age` is between 36 and 38', () => {
        beforeEach(() => {
          records = withAdapter.then(adapter =>
            adapter.find(type, null, {
              range: {
                age: [36, 38],
              },
            })
          );
        });
        it('should find only `john`', () => {
          expect(records).resolves.toEqual([
            expect.objectContaining({ name: 'john' }),
          ]);
        });
      });
    });
    describe('string', () => {
      describe('when `name` is between `i` and `k`', () => {
        beforeEach(() => {
          records = withAdapter.then(adapter =>
            adapter.find(type, null, {
              range: {
                name: ['i', 'k'],
              },
            })
          );
        });
        it('should find only `john`', () => {
          expect(records).resolves.toEqual([
            expect.objectContaining({ name: 'john' }),
          ]);
        });
      });
    });
  });
  afterAll(() => {
    teardown();
  });
});

describe('create', () => {
  let records;
  let withAdapter;
  beforeAll(() => {
    withAdapter = setup();
  });
  describe('with records', () => {
    beforeEach(() => {
      records = withAdapter.then(adapter => adapter.create(type, inputRecords));
    });
    it('should create no records', () => {
      expect(records).resolves.toEqual(expect.any(Object));
      expect(records.nodesCreated).resolves.toHaveLength(inputRecords.length);
    });
  });
  describe('without records', () => {
    beforeEach(() => {
      records = withAdapter.then(adapter => adapter.create(type, []));
    });
    it('should create no records', () => {
      expect(records).resolves.toEqual(expect.any(Array));
      expect(records).resolves.toHaveLength(0);
    });
  });
  afterAll(() => {
    teardown();
  });
});

function setup() {
  let adapter;
  try {
    adapter = new coreAdapterSingleton({
      recordTypes,
      message,
      adapter: [
        neo4jAdapter,
        {
          url: 'bolt://localhost',
          credentials: config,
          generateId: () =>
            Math.floor(Math.random() * Math.pow(2, 32)).toString(16),
        },
      ],
    });
  } catch (error) {
    return Promise.reject(error);
  }

  return adapter
    .connect()
    .then(() => adapter.delete(type))
    .then(() => adapter)
    .catch(error => {
      adapter.disconnect();
      throw error;
    });
}

function teardown() {
  let adapter;
  try {
    adapter = new coreAdapterSingleton({
      recordTypes,
      message,
      adapter: [
        neo4jAdapter,
        {
          url: 'bolt://localhost',
          credentials: config,
          generateId: () =>
            Math.floor(Math.random() * Math.pow(2, 32)).toString(16),
        },
      ],
    });
  } catch (error) {
    return Promise.reject(error);
  }

  adapter.connect().then(() => adapter.delete(type)).catch(error => {
    adapter.disconnect();
    throw error;
  });
}

function Integer(x) {
  return (x | 0) === x;
}
Integer.prototype = new Number();
