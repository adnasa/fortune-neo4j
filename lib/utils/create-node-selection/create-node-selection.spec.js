const createNodeSelection = require('./create-node-selection');

describe('createNodeSelection', () => {
  const name = 'user';
  const label = 'User';
  let matchParams;

  describe('with `matchParams`', () => {
    describe('when `matchParams` has scalar values', () => {
      beforeEach(() => {
        matchParams = {
          name: 'clark kent',
          age: 6,
        };
      });
      it('should return context and selection', () => {
        expect(createNodeSelection(name, label, matchParams)).toEqual({
          context: matchParams,
          selection: '(user:User {name: {name}, age: {age}})',
        });
      });
    });
    describe('when `matchParams` has list values', () => {
      beforeEach(() => {
        matchParams = {
          random: [1, 2],
        };
      });
      it('should return empty context', () => {
        expect(createNodeSelection(name, label, matchParams)).toEqual({
          context: {},
          selection: expect.any(String),
        });
      });
      it('should return selection', () => {
        expect(createNodeSelection(name, label, matchParams)).toEqual({
          context: {},
          selection: '(user:User)',
        });
      });
    });
  });

  describe('without `matchParams`', () => {
    it('should return empty context', () => {
      expect(createNodeSelection(name, label, matchParams)).toEqual({
        context: {},
        selection: expect.any(String),
      });
    });
    it('should return selection', () => {
      expect(createNodeSelection(name, label, matchParams)).toEqual({
        context: {},
        selection: '(user:User)',
      });
    });
  });
  describe('when `matchParams` is `null`', () => {
    beforeEach(() => {
      matchParams = null;
    });
    it('should return empty context', () => {
      expect(createNodeSelection(name, label, matchParams)).toEqual({
        context: {},
        selection: expect.any(String),
      });
    });
    it('should return selection', () => {
      expect(createNodeSelection(name, label, matchParams)).toEqual({
        context: {},
        selection: '(user:User)',
      });
    });
  });
});
