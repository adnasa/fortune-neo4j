function createNodeSelection(name, label, matchParams) {
  if (!matchParams) matchParams = {};
  const context = Object.entries(matchParams).reduce(
    (acc, [key, value]) =>
      !Array.isArray(value)
        ? Object.assign({}, acc, {
            [key]: value,
          })
        : acc,
    {}
  );
  const selections = Object.keys(context).reduce(
    (acc, key) => acc.concat(`${key}: {${key}}`),
    []
  );
  return {
    context,
    selection: selections.length
      ? `(${name}:${label} {${selections.join(', ')}})`
      : `(${name}:${label})`,
  };
}

module.exports = createNodeSelection;
