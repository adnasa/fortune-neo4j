// @TODO: Add support for an OR operator
function createRangeClause(name, params) {
  if (!params) params = {};
  const listOnly = Object.entries(params).reduce(
    (acc, [key, value]) =>
      Array.isArray(value)
        ? Object.assign({}, acc, {
            [key]: value,
          })
        : acc,
    {}
  );
  const clause = Object.entries(listOnly)
    .reduce((acc, [key, values], index) => {
      const [start, end] = values;
      const range = [];
      if (start !== null) range.push(`${name}.${key} >= {${key}${index}Start}`);
      if (end !== null) range.push(`${name}.${key} <= {${key}${index}End}`);
      return acc.concat(
        `${name}.${key} IS NOT NULL AND ${range.join(' AND ')}`
      );
    }, [])
    .join(' AND ');

  const context = Object.entries(listOnly).reduce(
    (acc, [key, values], index) =>
      values.reduce(
        (acc, value, _index) =>
          Object.assign({}, acc, {
            [`${key}${index}${_index === 0 ? 'Start' : 'End'}`]: value,
          }),
        acc
      ),
    {}
  );

  return {
    context,
    clause,
  };
}

module.exports = createRangeClause;
