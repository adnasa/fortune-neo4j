const createRangeClause = require('./create-range-clause');

describe('createRangeClause', () => {
  const name = 'user';
  let params;
  describe('when `params` has list values', () => {
    beforeEach(() => {
      params = {
        age: [0, 10],
      };
    });
    it('should return a context', () => {
      expect(createRangeClause(name, params)).toEqual({
        context: {
          age0Start: 0,
          age0End: 10,
        },
        clause: expect.any(String),
      });
    });
    it('should return a clause', () => {
      expect(createRangeClause(name, params)).toEqual({
        context: expect.any(Object),
        clause:
          'user.age IS NOT NULL AND user.age >= {age0Start} AND user.age <= {age0End}',
      });
    });
  });
  describe('when `params` has scalar values', () => {
    beforeEach(() => {
      params = {
        age: 100,
      };
    });
    it('should return an empty context', () => {
      expect(createRangeClause(name, params)).toEqual({
        context: {},
        clause: expect.any(String),
      });
    });
    it('should return an empty clause', () => {
      expect(createRangeClause(name, params)).toEqual({
        context: expect.any(Object),
        clause: '',
      });
    });
  });
});
