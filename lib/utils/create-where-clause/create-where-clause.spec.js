const createWhereClause = require('./create-where-clause');

describe('createWhereClause', () => {
  const name = 'user';
  let params;
  describe('when `params` has list values', () => {
    beforeEach(() => {
      params = {
        name: ['clark kent', 'bruce wayne'],
      };
    });
    it('should return a context', () => {
      expect(createWhereClause(name, params)).toEqual({
        context: {
          name0: 'clark kent',
          name1: 'bruce wayne',
        },
        clause: expect.any(String),
      });
    });
    it('should return a clause', () => {
      expect(createWhereClause(name, params)).toEqual({
        context: expect.any(Object),
        clause: 'user.name = {name0} OR user.name = {name1}',
      });
    });
  });
  describe('when `params` has scalar values', () => {
    beforeEach(() => {
      params = {
        name: 'clark kent',
      };
    });
    it('should return an empty context', () => {
      expect(createWhereClause(name, params)).toEqual({
        context: {},
        clause: expect.any(String),
      });
    });
    it('should return an empty clause', () => {
      expect(createWhereClause(name, params)).toEqual({
        context: expect.any(Object),
        clause: '',
      });
    });
  });
});
