// @TODO: add Array keys support
function createWhereClause(name, params, operator = 'OR') {
  if (!params) params = {};
  const listOnly = Object.entries(params).reduce(
    (acc, [key, value]) =>
      Array.isArray(value)
        ? Object.assign({}, acc, {
            [key]: value,
          })
        : acc,
    {}
  );
  const clause = Object.entries(listOnly)
    .reduce(
      (acc, [key, values], index) =>
        acc.concat(
          // user.name = {name0} OR user.name = {name1} OR
          // user.age = {age0} OR user.age = {age1}
          values.map((_, i) => `${name}.${key} = {${key}${i}}`)
        ),
      []
    )
    .join(` ${operator.trim()} `);

  const context = Object.entries(listOnly).reduce(
    (acc, [key, values], index) =>
      values.reduce(
        (acc, value, index) =>
          Object.assign({}, acc, {
            [`${key}${index}`]: value,
          }),
        acc
      ),
    {}
  );

  return { context, clause };
}

module.exports = createWhereClause;
