const withMatch = query => `MATCH ${query}`;
const withWhere = query => `WHERE ${query}`;

module.exports = {
  withMatch,
  withWhere,
};
